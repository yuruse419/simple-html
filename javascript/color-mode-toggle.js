// I certify that the HTML file I am submitting is all my own work.
// None of it is copied from any source or any person.
// Signed: Keagan Haar. 

// Author: Keagan Haar
// Date: 08/13/2023
// Class: CSC135
// Project: Final
// Description: Final project.


// Start main function
function toggleMode() {
  var i

  var darkColorOne = Array.from(document.getElementsByClassName('dark-color-scheme-color-one'))
  var darkColorTwo = Array.from(document.getElementsByClassName('dark-color-scheme-color-two'))
  var darkColorThree = Array.from(document.getElementsByClassName('dark-color-scheme-color-three'))
  var lightColorOne = Array.from(document.getElementsByClassName('light-color-scheme-color-one'))
  var lightColorTwo = Array.from(document.getElementsByClassName('light-color-scheme-color-two'))
  var lightColorThree = Array.from(document.getElementsByClassName('light-color-scheme-color-three'))
  var darkText = Array.from(document.getElementsByClassName('dark-text-color'))
  var lightText = Array.from(document.getElementsByClassName('light-text-color'))

  for(i = 0; i < darkColorOne.length; i++) {
    darkColorOne[i].classList.toggle('dark-color-scheme-color-one')
    darkColorOne[i].classList.toggle('light-color-scheme-color-one')
  }

  for(i = 0; i < darkColorTwo.length; i++) {
    darkColorTwo[i].classList.toggle('dark-color-scheme-color-two')
    darkColorTwo[i].classList.toggle('light-color-scheme-color-two')
  }

  for(i = 0; i < darkColorThree.length; i++) {
    darkColorThree[i].classList.toggle('dark-color-scheme-color-three')
    darkColorThree[i].classList.toggle('light-color-scheme-color-three')
  }

  for(i = 0; i < lightColorOne.length; i++) {
    lightColorOne[i].classList.toggle('dark-color-scheme-color-one')
    lightColorOne[i].classList.toggle('light-color-scheme-color-one')
  }

  for(i = 0; i < lightColorTwo.length; i++) {
    lightColorTwo[i].classList.toggle('dark-color-scheme-color-two')
    lightColorTwo[i].classList.toggle('light-color-scheme-color-two')
  }

  for(i = 0; i < lightColorThree.length; i++) {
    lightColorThree[i].classList.toggle('dark-color-scheme-color-three')
    lightColorThree[i].classList.toggle('light-color-scheme-color-three')
  }

  for(i = 0; i < darkText.length; i++) {
    darkText[i].classList.toggle('dark-text-color')
    darkText[i].classList.toggle('light-text-color')
  }

  for(i = 0; i < lightText.length; i++) {
    lightText[i].classList.toggle('dark-text-color')
    lightText[i].classList.toggle('light-text-color')
  }
}
